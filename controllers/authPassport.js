const { UserGame } = require("../models");
const passport = require("passport");

module.exports = {
  registerPassport: (req, res, next) => {
    UserGame.register(req.body)
      .then(() => {
        res.redirect("/view/login");
      })
      .catch((err) => next(err));
  },
  loginPassport: passport.authenticate("local", {
    successRedirect: "/",
    failureRedirect: "/view/login",
    failureFlash: true,
  }),
};
