const express = require("express");
const app = express();
const router = require("./routers");
const swaggerJSON = require("./swagger.json");
const swaggerUI = require("swagger-ui-express");
const bodyParser = require("body-parser");
const passport = require("passport");
const session = require("express-session");
const flash = require("express-flash");
require("./lib/passport-local");
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(
  session({
    secret: "zaky1234",
    resave: false,
    saveUninitialized: false,
  })
);
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
app.use(bodyParser.json());
app.set("view engine", "ejs"); // set ejs
app.use(express.static("public"));
app.use("/docs", swaggerUI.serve, swaggerUI.setup(swaggerJSON));

app.use(router);

app.get("/", (req, res) => {
  res.render("index");
});

module.exports = app;
