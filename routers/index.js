const router = require("express").Router();
const userGame = require("./usergame.route");
const userGameBiodata = require("./usergamebiodata.route");
const userGameHistory = require("./usergamehistory.route");
const auth = require("../middleware/auth");
const passport = require("passport");
const authPassport = require("../controllers/authPassport");
const { register, login } = require("../controllers/auth");

function checkAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }

  res.redirect("/view/login");
}

function checkNotAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return res.redirect("/");
  }
  next();
}

// HOME PAGE
router.get("/", (req, res) => res.render("index", { isAuthenticated: req.isAuthenticated() }));

// REGISTER PAGE
router.get("/view/register", checkNotAuthenticated, (req, res) => res.render("register"));
router.post("/view/register", checkNotAuthenticated, authPassport.registerPassport);

// LOGIN PAGE
router.get("/view/login", checkNotAuthenticated, (req, res) => res.render("login"));
router.post("/view/login", checkNotAuthenticated, authPassport.loginPassport);

// VIEW USER GAME
router.get("/view/user-games", checkAuthenticated, userGame);
router.get("/view/create-user-games", checkAuthenticated, userGame);
router.post("/view/create-user-games", checkAuthenticated, userGame);
router.get("/view/update-form-user-games/:id", checkAuthenticated, userGame);
router.post("/view/update-user-games/:id", checkAuthenticated, userGame);
router.get("/view/delete-user-games/:id", checkAuthenticated, userGame);

// VIEW USER GAME BIODATA
router.get("/view/user-games-biodata", checkAuthenticated, userGameBiodata);
router.get("/view/create-user-games-biodata", checkAuthenticated, userGameBiodata);
router.post("/view/create-user-games-biodata", checkAuthenticated, userGameBiodata);
router.get("/view/update-form-user-games-biodata/:id", checkAuthenticated, userGameBiodata);
router.post("/view/update-user-games-biodata/:id", checkAuthenticated, userGameBiodata);
router.get("/view/delete-user-games-biodata/:id", checkAuthenticated, userGameBiodata);

// VIEW USER GAME HISTORY
router.get("/view/user-games-history", checkAuthenticated, userGameHistory);
router.get("/view/create-user-games-history", checkAuthenticated, userGameHistory);
router.post("/view/create-user-games-history", checkAuthenticated, userGameHistory);
router.get("/view/update-form-user-games-history/:id", checkAuthenticated, userGameHistory);
router.post("/view/update-user-games-history/:id", checkAuthenticated, userGameHistory);
router.get("/view/delete-user-games-history/:id", checkAuthenticated, userGameHistory);

router.get("/view/logout", (req, res) => {
  req.logout(function (err) {
    if (err) {
      return next(err);
    }
    res.redirect("/view/login");
  });
});

router.all("*", (req, res) => {
  res.redirect("/view/login");
});

// user_games endppoint
router.use("/api/get-user-games", auth, userGame);
router.use("/api/get-user-games/:id", auth, userGame);
router.use("/api/create-user-games", auth, userGame);
router.use("/api/update-user-games", auth, userGame);
router.use("/api/delete-user-games", auth, userGame);

// user_game_biodata endppoint
router.use("/api/get-user-biodata", auth, userGameBiodata);
router.use("/api/get-user-biodata/:id", auth, userGameBiodata);
router.use("/api/create-user-biodata", auth, userGameBiodata);
router.use("/api/update-user-biodata", auth, userGameBiodata);
router.use("/api/delete-user-biodata", auth, userGameBiodata);

// user_game_history endppoint
router.use("/api/get-user-history", auth, userGameHistory);
router.use("/api/get-user-history/:id", auth, userGameHistory);
router.use("/api/create-user-history", auth, userGameHistory);
router.use("/api/update-user-history", auth, userGameHistory);
router.use("/api/delete-user-history", auth, userGameHistory);

router.post("/api/register", register);
router.post("/api/login", login);

module.exports = router;
